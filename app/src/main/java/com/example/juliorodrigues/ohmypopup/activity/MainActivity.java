package com.example.juliorodrigues.ohmypopup.activity;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.juliorodrigues.ohmypopup.R;
import com.example.juliorodrigues.ohmypopup.dialog.JogosPersonagensDialogFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends ActionBarActivity {

    @OnClick(R.id.jogosButton)
    public void onClickJogosButton() {
        FragmentManager fragmentManager = getFragmentManager();
        JogosPersonagensDialogFragment jogosPersonagensDialogFragment = new JogosPersonagensDialogFragment();
        jogosPersonagensDialogFragment.show(fragmentManager, JogosPersonagensDialogFragment.class.getCanonicalName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        onClickJogosButton();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
