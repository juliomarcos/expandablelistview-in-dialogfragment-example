package com.example.juliorodrigues.ohmypopup.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.juliorodrigues.ohmypopup.model.Jogo;
import com.example.juliorodrigues.ohmypopup.model.Personagem;

import java.util.List;

/**
 * Created by juliorodrigues on 17/01/15.
 */
public class JogosAdapter extends BaseExpandableListAdapter {

    public static final int ENTRY_PADDING = 18;
    private final Context context;
    private final List<Jogo> jogos;

    public JogosAdapter(Context context, List<Jogo> jogos) {
        this.context = context;
        this.jogos = jogos;
    }

    @Override
    public int getGroupCount() {
        return jogos.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return jogos.get(groupPosition).personagens.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return jogos.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return jogos.get(groupPosition).personagens.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition * 1000 + childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = new TextView(context);
            convertView.setPadding((int) (ENTRY_PADDING*3f), ENTRY_PADDING, ENTRY_PADDING, ENTRY_PADDING);
        }
        TextView jogoView = (TextView) convertView;
        jogoView.setTextSize(19f);
        Jogo jogo = (Jogo) getGroup(groupPosition);
        jogoView.setText(jogo.nome);
        return jogoView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = new TextView(context);
            convertView.setPadding((int) (ENTRY_PADDING*4.5f), ENTRY_PADDING, ENTRY_PADDING, ENTRY_PADDING);
        }
        TextView personagemView = (TextView) convertView;
        personagemView.setTypeface(personagemView.getTypeface(), Typeface.ITALIC);
        Personagem personagem = (Personagem) getChild(groupPosition, childPosition);
        personagemView.setText(personagem.nome);
        return personagemView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
