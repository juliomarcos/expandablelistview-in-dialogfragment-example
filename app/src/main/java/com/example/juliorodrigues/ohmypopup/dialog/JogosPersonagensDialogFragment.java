package com.example.juliorodrigues.ohmypopup.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;

import com.example.juliorodrigues.ohmypopup.R;
import com.example.juliorodrigues.ohmypopup.adapter.JogosAdapter;
import com.example.juliorodrigues.ohmypopup.model.Jogo;
import com.example.juliorodrigues.ohmypopup.model.Personagem;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by juliorodrigues on 17/01/15.
 */
public class JogosPersonagensDialogFragment extends DialogFragment {

    private ArrayList<Jogo> jogos;

    public JogosPersonagensDialogFragment() {
        Personagem akara = new Personagem("Akara");
        Personagem cain = new Personagem("Cain");
        Personagem charsi = new Personagem("Charsi");
        Personagem flavie = new Personagem("Gheed");
        List<Personagem> personagemListDiablo2 = new ArrayList<Personagem>();
        personagemListDiablo2.add(akara);
        personagemListDiablo2.add(cain);
        personagemListDiablo2.add(charsi);
        personagemListDiablo2.add(flavie);
        Jogo diablo2 = new Jogo("Diablo II", personagemListDiablo2);

        jogos = new ArrayList<Jogo>();
        jogos.add(diablo2);
        jogos.add(diablo2);
        jogos.add(diablo2);
        jogos.add(diablo2);
        jogos.add(diablo2);
        jogos.add(diablo2);
        jogos.add(diablo2);
        jogos.add(diablo2);
        jogos.add(diablo2);
        jogos.add(diablo2);
        jogos.add(diablo2);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        Window window = dialog.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setBackgroundDrawable(new ColorDrawable(0));

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_filtrar, container, false);
        ButterKnife.inject(this, view);

        ExpandableListView jogosListView = (ExpandableListView) view.findViewById(R.id.jogosList);
        JogosAdapter jogosAdapter = new JogosAdapter(getActivity(), this.jogos);
        jogosListView.setAdapter(jogosAdapter);

        return view;
    }

    @OnClick(R.id.cancelarButton)
    public void onClickCancelar() {
        dismiss();
    }
}
