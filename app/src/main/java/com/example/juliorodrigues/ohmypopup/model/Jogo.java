package com.example.juliorodrigues.ohmypopup.model;

import java.util.List;

/**
 * Created by juliorodrigues on 17/01/15.
 */
public class Jogo {
    public final String nome;
    public final List<Personagem> personagens;

    public Jogo(String nome, List<Personagem> personagens) {
        this.nome = nome;
        this.personagens = personagens;
    }
}
