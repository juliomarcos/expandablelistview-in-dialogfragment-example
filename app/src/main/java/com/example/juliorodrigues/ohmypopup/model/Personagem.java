package com.example.juliorodrigues.ohmypopup.model;

/**
 * Created by juliorodrigues on 17/01/15.
 */
public class Personagem {
    public final String nome;

    public Personagem(String nome) {
        this.nome = nome;
    }
}
